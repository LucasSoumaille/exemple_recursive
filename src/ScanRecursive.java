
public class ScanRecursive {
	

	public Node putValue(int[] listValue) {
		
		Node root = null;
		
		for (int i = 0; i < listValue.length; i++) {
			if (i == 0) {
				root = new Node(listValue[i]);
			} else {
				 recursive(root, listValue[i]);
			}
			
		}
		return root;
		
	}
	
	public void recursive(Node node, int value) {
		
		if (value < node.getValue()) {
			if (node.getLeftChild() != null) {
				recursive(node.getLeftChild(), value);
			} else {
				Node newNode = new Node(value);
				node.setLeftChild(newNode);
			}
		
		} else {
			
			if (node.getRightChild() != null) {
				recursive(node.getRightChild(), value);
			} else {
				Node newNode = new Node(value);
				node.setRightChild(newNode);
			}
			
		}
		
	}
	 
	public void returnValue(Node node) {
		System.out.println("Valeur du noeud en cours : " + node.getValue());
		if (!(node == null)) {
			if (node.getLeftChild() != null ) {
				System.out.println("Valeur du noeud inférieur gauche : " + node.getLeftChild().getValue());
				returnValue(node.getLeftChild());	
			} else {
				System.out.println("Noeud inférieur gauche vide");
			}
			
			if (node.getRightChild() != null ) {
				System.out.println("Valeur du noeud inférieur droit : " + node.getRightChild().getValue());
				returnValue(node.getRightChild());	
			} else {
				System.out.println("Noeud inférieur droit vide");
			}
		}
		
	}

}
