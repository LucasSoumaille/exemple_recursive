
public class Node {
	
	private int value;
	private Node leftChildNode;
	private Node rightChildNode;

	public Node(int pValue) {
		this.value = pValue;
	}

	public Node() {
	}
	
	public int getValue() {
		return this.value;
	}
	
	public void setValue(int pValue) {
		this.value = pValue;
	}
	
	public void setLeftChild (Node pNode) {
		this.leftChildNode = pNode;
	}
	
	public void setRightChild (Node pNode) {
		this.rightChildNode = pNode;
	}
	
	public Node getLeftChild() {
		return leftChildNode;
	}
	
	public Node getRightChild() {
		return rightChildNode;
	}

}
